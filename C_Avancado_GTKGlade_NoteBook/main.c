#include <stdlib.h>
#include <gtk/gtk.h>

GtkBuilder *builder;
GtkWidget *window;

G_MODULE_EXPORT void aba1(){ //funcoes apenas para imprimir no console qual botao foi clicado

    g_print("Botao1\n");
}

G_MODULE_EXPORT void aba2(){

    g_print("Botao2\n");
}

G_MODULE_EXPORT void aba3(){

    g_print("Botao3\n");
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
