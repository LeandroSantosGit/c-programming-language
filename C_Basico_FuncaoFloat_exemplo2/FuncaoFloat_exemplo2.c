#include<stdio.h>

float Soma(float numero1, float numero2);

void main(){

    float x = 20.5;
    float y = 12.5;

    float resultado = Soma(x, y);

    printf("Valor da soma: %.2f\n", resultado);

}

float Soma(float numero1,float numero2){

    return numero1 + numero2;
}
