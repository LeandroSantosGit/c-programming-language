#include <stdlib.h>
#include <gtk/gtk.h>

void Fechar(){

    gtk_main_quit();
}

void Titulo(GtkWidget *w, gpointer p){

    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w))){ //se titulo estiver assinalado o titulo da janela e checkButton
        gtk_window_set_title(p, "CheckButton");
    }else{                                //se nao, nao sera nada
        gtk_window_set_title(p, "");
    }
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *fixedC;
    GtkWidget *check;

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Leandro");
    gtk_window_set_default_size(GTK_WINDOW(janela), 800, 600);
    gtk_window_set_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    fixedC = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(janela), fixedC);

    check = gtk_check_button_new_with_label("Titulo"); //criar check button
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), TRUE); //faz com que o widget check button seja assinalado
    GTK_WIDGET_UNSET_FLAGS(check, GTK_CAN_FOCUS); //retirar foco do widget check
    gtk_fixed_put(GTK_FIXED(fixedC), check, 80, 80); //ajusta posicao do widget no container

    g_signal_connect(check, "clicked", GTK_SIGNAL_FUNC(Titulo),(gpointer) janela); //dar sinal ao widget check

    gtk_widget_show_all(janela);

    gtk_main();

    return 0;
}
