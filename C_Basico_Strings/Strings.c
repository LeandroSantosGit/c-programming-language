#include<stdio.h>
#include<string.h>

void main(){

    char str1[10] = "Leandro";
    char str2[10] = "Treinaweb";

    printf("A string str1 e: %s\n\nA string str2 e: %s\n\n", str1, str2);

    printf("Agora vamos mudar as funcoes\n\n");

    strcpy(str1, str2); //copia a 2opcao para a primeira
    printf("str1: %s\n\n", str1);
    printf("str2: %s\n\n", str2);

    strcat(str1, str2); //concatena
    printf("str1: %s\n\n", str1);
    printf("str2: %s\n\n", str2);
}
