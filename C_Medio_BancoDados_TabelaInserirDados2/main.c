#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

int main(int argc, char **argv)
{
    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "INSERT INTO Cachorro(nome, raca) VALUES('toto', 'vira lata')")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if (mysql_query(conn, "INSERT INTO Cachorro(nome,raca)VALUES ('duke','pastor alemao')")){
      fprintf(stderr, "%s\n", mysql_error(conn));
      mysql_close(conn);
      exit(1);
    }

    if (mysql_query(conn, "INSERT INTO Cachorro(nome,raca)VALUES ('fifi','poodle')")){
      fprintf(stderr, "%s\n", mysql_error(conn));
      mysql_close(conn);
      exit(1);
    }

    mysql_close(conn);
    exit(0);
}
