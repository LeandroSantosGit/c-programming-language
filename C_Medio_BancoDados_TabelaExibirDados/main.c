#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

int main(int argc, char **argv)
{
    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }
                        // SELECT selecionar dados da tabela
    if(mysql_query(conn, "SELECT * FROM Cachorro")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    MYSQL_RES *result = mysql_store_result(conn); //ponteiro ira receber o resultado da pesquisa

    if(result == NULL){  //verificar se retornou algum valor
        fprintf(stderr, "%s\n", mysql_error(conn)); //se nao fechar aplicacao
        mysql_close(conn);
        exit(1);
    }

    int num_fields = mysql_fetch_row(result); //receber resultados do numero das colunas
    MYSQL_ROW row; //ira ajudar a busca por resgistros
    int i;  //auxiliar no laco for

    while((row = mysql_fetch_row(result))){ //enquanto a busca por registro existir
        for(i = 0; i < num_fields; i++){  //exibir dados da tabela no console
            printf("%s ", row[i] ? row[i] : "NULL");
        }
        printf("\n");
    }

    mysql_free_result(result);
    mysql_close(conn);
    exit(0);
}
