#include<stdio.h>

int main(){

    FILE *fp;
    char string[50];

    fp = fopen("Arquivo.txt", "w");

    //capturar os caractres digitados
    do{
        printf("Digite uma mensaguem\n");
        gets(string);
        fputs(string, fp);
        putc("\n", fp);

        //verifica se existe algum erro na gravacao do arquivo
        if(ferror(fp)){
            perror("Erro ao gravar arquivo.");
            fclose(fp);
        }

    }while(strlen(string) > 1);

    fclose(fp);
}
