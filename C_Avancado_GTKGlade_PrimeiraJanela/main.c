#include <stdlib.h>
#include <gtk/gtk.h>

GtkBuilder *builder;  //ponteiro para acessar interface do glade
GtkWidget *window; //ponteiro para janela

int main(int argc, char **argv){

    gtk_init(&argc, &argv); //iniciar GTK
    builder = gtk_builder_new(); //criar builder
    gtk_builder_add_from_file(builder, "Glade1.glade", NULL); //opontar o arguivo que contem o glade
    window = GTK_WIDGET(gtk_builder_get_object(builder, "Janela")); //obter o ponteiro da janela principal
    gtk_builder_connect_signals(builder, NULL); //adicinando sinais
    g_object_unref(G_OBJECT(builder)); //destruindo o builder
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
