#include <stdio.h>
#include <stdlib.h>

struct NO{

    int numero;
    struct NO *proximo;
};

typedef struct NO no;

no *RetiradaInicio(no *LISTA){

    if(LISTA->proximo == NULL){
        printf("Lista esta vazia!\n");
    }else{
        no *temp = LISTA->proximo;
        LISTA->proximo = temp->proximo;
    }
}

no *RetiradaFinal(no *LISTA){

    if(LISTA->proximo == NULL){
        printf("Lista esta vazia!\n");
    }else{
        no *ultimo = LISTA->proximo;
        no *penultimo = LISTA;

        while(ultimo->proximo != NULL){
            penultimo = ultimo;
            ultimo = ultimo->proximo;
        }
        penultimo->proximo = NULL;
    }
}

void Opcoes(no *LISTA, int op){

    switch(op){
        case 0:
            Liberar(LISTA);
            break;
        case 1:
            InserirInicio(LISTA);
            break;
        case 2:
            InserirFinal(LISTA);
            break;
        case 3:
            RetiradaInicio(LISTA);
            break;
        case 4:
            RetiradaFinal(LISTA);
            break;
        case 5:
            Exibir(LISTA);
            break;
        default:
            printf("Comando Invalido!\n\n");
    }
}

int Vazia(no *LISTA){

    if(LISTA->proximo == NULL){
        return 1;
    }else{
        return 0;
    }
}

no *Alocacao(){

    no *novo = malloc(sizeof(no));

    if(!novo){
        printf("\nSem Memoria Disponivel!\n");
        exit(1);
    }else{
        printf("\nNovo Elemento na Lista: ");
        scanf("%d", &novo->numero);
        return novo;
    }
}

void InserirInicio(no *LISTA){

    no *novo = Alocacao();
    no *temp = LISTA->proximo;

    LISTA->proximo = novo;
    novo->proximo = temp;
}

void InserirFinal(no *LISTA){

    no *novo = Alocacao();
    novo->proximo = NULL;

    if(Vazia(LISTA)){
        printf("\nLista Vazia!\n");
    }else{
        no *temp = LISTA->proximo;

        while(temp->proximo != NULL)
            temp = temp->proximo;

        temp->proximo = novo;
    }
}

 void Exibir(no *LISTA){

    if(Vazia(LISTA)){
        printf("\nLista Vazia!\n\n");
        return ;
    }

    no *temp;
    temp = LISTA->proximo;
    printf("\nLista: ");

    while(temp != NULL){
        printf("%5d", temp->numero);
        temp = temp->proximo;
    }
    printf("\n\n");
 }

void Liberar(no *LISTA){

    if(!Vazia(LISTA)){
        no *proxNode,
            *atual;

            atual = LISTA->proximo;
            while(atual != NULL){
                proxNode = atual->proximo;
                free(atual);
                atual = proxNode;
            }
    }
}



int main(void)
{
    int opt;

    no *LISTA = malloc(sizeof(no));

    if(!LISTA){
        printf("Sem Memoria!\n");
        exit(1);
    }else{
        LISTA->proximo = NULL;

        do{
            printf("Opcoes\n");
            printf("0 - Sair\n");
            printf("1 - Inserir Inicio\n");
            printf("2 - Inserir Final\n");
            printf("3 - Retirar Inicio\n");
            printf("4 - Retirar Final\n");
            printf("5 - Exibir\n");
            printf("Opcoes: ");
            scanf("%d", &opt);

        Opcoes(LISTA, opt);
        }while(opt);

        free(LISTA);
        return 0;
    }
}
