#include<stdio.h>

void main(){

    int x = 10;
    int y = 15;

    printf("Valores: x = %i e y = %i\n\n", x, y);

    valores(&x,&y);
    printf("Valores invertidos: x = %i e y = %i\n", x, y);
}

void valores(int *a, int *b){
    //funcao para inverter valores
    int temp = *a;

    *a = *b;

    *b = temp;
}
