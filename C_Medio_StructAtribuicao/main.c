#include <stdio.h>
#include <stdlib.h>

//Atribuicao de estrutura

    struct{

        int numero;

    }loja1, loja2;

int main()
{
    loja1.numero = 20;
    loja2 = loja1; //loja2 passa a receber mesmo valor de loja1

    printf("Loja1: %d\n", loja1.numero);
    printf("Loja2: %d\n", loja2.numero);

    return 0;
}

