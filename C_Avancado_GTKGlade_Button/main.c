#include <stdlib.h>
#include <gtk/gtk.h>

//botao pra fechar aplicacao e botao para verificar se botao esta ativo ou desativado

GtkWidget *window;
GtkBuilder *builder;
GtkWidget *toggle;

G_MODULE_EXPORT void Ola(gpointer t){  //funcao para montrar se botao esta ativo ou desativado

    if(gtk_toggle_button_get_active(t) == 1){
        g_print("Ligado");
    }else{
        g_print("Desligado");
    }
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade2.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "Janela2"));
    toggle = GTK_WIDGET(gtk_builder_get_object(builder, "togglebutton1")); //informar o botao toggle no glade
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
