#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Strlen */ //funcao para returnar quantidade de caracteres uma string tem
/*
int main()
{
    char *nome = "Leandro";
    int valor = strlen(nome); //passando a quantidade de caracteres para variavel valor

    printf("%d\n", valor);

    return 0;
} */


/* strcmp */ //compara duas strings e returna numero um valor inteiro
                 //se retornar uma volor negativo e pk a primeira string e menor que a segunda
                 //se retornar uma volor positivo e pk a primeira string e maior que a segunda
                 //se retornar um zero as strings sao identicas
/*
int main(){

    char *nome1 = "Leandro";
    char *nome2 = "Leand";

    int valor = strcmp(nome1, nome2);

    printf("%d\n", valor);

    return 0;
} */


/* Strchr */ //localizar um determinado caracter e exibir apartir do primeiro caracter
/*
int main(){

    char *nome = "Meu nome e Leandro";
    char *s;

    s = strchr(nome, 'e');

    printf("%s\n", s);

    return 0;
} */


/* Strrchr */ //localizar um determinado caracter e exibir apartir do ultimo caracter
/*
int main(){

    char *nome = "Meu nome e Leandro";
    char *s;

    s = strrchr(nome, 'm');

    printf("%s\n", s);

    return 0;
} */


/* Strspn */ //retorna valores incomun
/*
int main(){

    char *nome = "125phn";
    char *s = "1234567890";
    int i;

    i = strspn(nome, s);

    printf("%d\n", i);

    return 0;
} */


/* Strstr */ //retornar apartir da primeira ocorrencia de uma determinada string
/*
int main(){

    char *numero = "8";
    char *s = "1234567890";
    char *i;

    i = strstr(s,numero);

    printf("%s\n", i);

    return 0;
} */



