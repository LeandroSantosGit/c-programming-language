#include<stdio.h>

void main(){

    FILE *fp;
    char string[50];

    //abrir arquivo
    fp = fopen("Arquivo.txt", "r");

    //escrever e guardar a string digitada em um arquivo
    printf("Escreva uma mensagem\n");
    gets(string);
    fputs(string,fp);

    printf("\n\nVamos fazer a leitura do arquivo.\n");
    fgets(string, 50, fp); //ler a string de um arquivo

    printf("%s\n", string);

    fclose(fp);
}
