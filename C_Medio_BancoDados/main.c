#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

void Selecionar(){

    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "SELECT * FROM cursos")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    MYSQL_RES *result = mysql_store_result(conn);

    if(result == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    int num_fields = mysql_num_fields(result);
    MYSQL_ROW row;
    int i;

    while((row = mysql_fetch_row(result))){
        for(i = 0; i < num_fields; i++){
            printf("%s ", row[i] ? row[i] : "NULL");
        }
        printf("\n");
    }

    mysql_free_result(result);
}

void Inserir(){

    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "INSERT INTO cursos(id, nome, modulo) VALUES(1, 'cpp', 'basico')")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "INSERT INTO cursos(id, nome, modulo) VALUES(2, 'ctt', 'intermediario')")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }
}

int main(int argc, char **argv){

    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    int opt;
    printf("Escola 1-Inserir, 2-Selecionar, 0-Sair");
    scanf("%d", &opt);

        switch(opt){
            case 1:
                Inserir();
                printf("Inserir um valor no banco de dados");
                break;
            case 2:
                Selecionar();
                break;
            case 3:
                exit(1);

            default:
                printf("Opcao Invalida!");
        }

        mysql_close(conn);
        exit(0);
}
