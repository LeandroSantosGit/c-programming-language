#include <stdio.h>
#include <stdlib.h>


void Menu(){

    printf("        Menu               \n");
    printf("1 - Acessar Numero de IP\n");
    printf("2 - Acessar Versao do Sistema\n");
    printf("3 - Acessar Informacoes do Sistema\n");
    printf("4 - Acessar Portas do Sistema\n");
    printf("5 - Acessar Central de Seguranca\n");
    printf("6 - Verificar Erros no Sistema\n");
    printf("7 - Reiniciar Maquina\n");
    printf("8 - Desligar Maquina\n");
    printf("0 - Sair\n");
}
int main()
{
    int opcoes;

    Inicio:
        printf("\n\n");
        Menu();
        printf("\n\n");

        while(opcoes != 0){
            printf("Digite Opcao: ");
            scanf("%d", &opcoes);

            switch(opcoes){
                case 1:
                    system("ipconfig\n");
                    goto Inicio;
                    break;
                case 2:
                    system("ver\n");
                    goto Inicio;
                    break;
                case 3:
                    system("systeminfo\n");
                    goto Inicio;
                    break;
                case 4:
                    system("netstat\n");
                    goto Inicio;
                    break;
                case 5:
                    system("wscui.cpl\n");
                    goto Inicio;
                    break;
                case 6:
                    system("sfc /scannow\n");
                    goto Inicio;
                    break;
                case 7:
                    system("shutdown -r -t 0\n");
                    goto Inicio;
                    break;
                case 8:
                    system("shutdown -s -t 0\n");
                    goto Inicio;
                    break;
                case 0:
                    printf("Saindo do Sistema!\n");
                    exit(1);
                default:
                    goto Inicio;
            }
        }
        return 0;
}
