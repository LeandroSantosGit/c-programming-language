#include <stdio.h>
#include <stdlib.h>

//union e uma unica posicao de memoria compartilhada por diversa variaveis e tipos diferentes
union tipoValores{

    float numero1f;
    int numero2i;
};

typedef union tipoValores tval;

void quadradof(float a){

    printf("Resultado: %f\n", a * a);
}

void quadradoi(int a){

    printf("Resultado: %d\n", a * a);
}

int main(){

    tval p1;
    int op;

    printf("Digite numero 1 ou Numero 2: \n");
    scanf("%d", &op);

    if(op == 1){
        p1.numero1f = 2.5;
        quadradof(p1.numero1f);
    }else{
        p1.numero2i = 2;
        quadradoi(p1.numero2i);
    }

    return 0;
}
