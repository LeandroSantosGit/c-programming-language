#include <stdio.h>
#include <stdlib.h>

//enum e define uma variavel pra receber um conjunto de valores restritos inteiros
enum dias_semana {Segunda = 1, Terca, Quarta, Quinta, Sexta, Sabado, Domingo}dias;


int main()
{
    printf("Digite numero do dia da semana: ");
    scanf("%d", &dias);

    switch(dias){

        case Segunda:
            printf("%d - Segunda\n", dias);
            break;
        case Terca:
            printf("%d - Terca\n", dias);
            break;
        case Quarta:
            printf("%d - Quarta\n", dias);
            break;
        case Quinta:
            printf("%d - Quinta\n", dias);
            break;
        case Sexta:
            printf("%d - Sexta\n", dias);
            break;
        case Sabado:
            printf("%d - Sabado\n", dias);
            break;
        case Domingo:
            printf("%d - Domingo\n", dias);
            break;
        default:
            printf("Opcao Invalida!");
            exit(1);
    }

    return 0;
}
