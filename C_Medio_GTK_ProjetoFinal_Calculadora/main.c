#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <string.h>

GtkWidget *janela;
GtkWidget *fixedC;
GtkWidget *botao0, *botao1, *botao2, *botao3, *botao4, *botao5, *botao6, *botao7, *botao8, *botao9;
GtkWidget *botaoSoma, *botaoDiv, *botaoMult, *botaoSub, *botaoIgual, *botaoC;
GtkWidget *caixa;

char *valorc1 = NULL;
char *valorc2 = NULL;
char valorc3[];
int valori1 = NULL;
int valori2 = NULL;
int valori3 = NULL;
char valores1[];
char valores2[];

void Fechar(){ //fechar aplicacao

    gtk_main_quit();
}

void pegarValor1(){
                //pegar o primeiro valor digitado na caixa de texto
    valorc1 = gtk_entry_get_text(caixa);
}

void pegarValor2(){
                ////pegar o segundo valor digitado na caixa de texto
    valorc2 = gtk_entry_get_text(caixa);
}

void concat(GtkWidget *w, gpointer p){ //gpointer retorna valores dos botoes

    if(valori1 == NULL){
        strcat(valores1, p); //concatenar valores
        valUm();
    }
    else
    {
        strcat(valores2, p); //concatenar valores
        valDois();
    }
}

void valUm(){
             //colocar valore na caixa de texto
    gtk_entry_set_text(caixa, valores1);
    pegarValor1();  //colocar valor na variavel pegarValor
}

void valDois(){
            //colocar valores na caixa de texto
    gtk_entry_set_text(caixa, valores2);
    pegarValor2(); //colocar valor na variavel pegarValor
}

char *op;

void Operacao(GtkWidget *w, gpointer p){  //retornar opracao

    if(valori1 == NULL){
        valori1 = atoi(valorc1); //converter valor char para int
        gtk_entry_set_text(caixa,"");
    }

op = p;
}

void Soma(int a, int b){

    valori3 = a + b;
    itoa(valori3, valorc3, 10); //converter int para char pra exibir o resultado na caixa de texto
    gtk_entry_set_text(caixa, valorc3);
}

void Subtracao(int a, int b){

    valori3 = a - b;
    itoa(valori3, valorc3, 10);
    gtk_entry_set_text(caixa, valorc3);
}

void Multiplicacao(int a, int b){

    valori3 = a * b;
    itoa(valori3, valorc3, 10);
    gtk_entry_set_text(caixa, valorc3);
}

void Divisao(int a, int b){

    if(b != 0){ //verificar se e possivel dividir
        valori3 = a / b;
        itoa(valori3, valorc3, 10);
        gtk_entry_set_text(caixa, valorc3);
    }
}

void Limpeza(){  //limpar todas variaveis

    valorc1 = NULL;
    valorc2 = NULL;
    valori1 = NULL;
    valori2 = NULL;
    valori3 = NULL;

    int size1 = strlen(valores1);
    int i;

    for(i = 0; i < size1; i++){
        valores1[i] = NULL;
    }

    int size2 = strlen(valores2);

    for(i = 0; i < size2; i++){
        valores2[i] = NULL;
    }
}

void LimparT(){  //limpeza total

    valorc1 = NULL;
    valorc2 = NULL;
    valori1 = NULL;
    valori2 = NULL;
    valori3 = NULL;

    int size1 = strlen(valores1);
    int i;

    for(i = 0; i < size1; i++){
        valores1[i] = NULL;
    }

    int size2 = strlen(valores2);

    for(i = 0; i < size2; i++){
        valores2[i] = NULL;
    }
    gtk_entry_set_text(caixa,""); //limpar caixa de texto
}

void Resultado(){

    if(valori2 == NULL){ //verificar variavel
        valori2 = atoi(valorc2); //converter string(char) para integer(int)
        gtk_entry_set_text(caixa, "");
    }

    if(op == "Soma"){ //verifica se � igual a soma
        Soma(valori1, valori2); //chama funcao soma
        Limpeza(); //chamar funcao limpeza
    }

    if(op == "Subtracao"){
        Subtracao(valori1, valori2);
        Limpeza();
    }

    if(op == "Divisao"){
        Divisao(valori1, valori2);
        Limpeza();
    }

    if(op == "Multiplicacao"){
        Multiplicacao(valori1, valori2);
        Limpeza();
    }
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "CALCULADORA");
    gtk_window_set_default_size(GTK_WINDOW(janela), 350, 200);
    gtk_window_set_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    //container
    fixedC = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(janela), fixedC);

    //caixa de texto
    caixa = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), caixa, 10, 10);

    //botoes de numeros
    botao1 = gtk_button_new_with_label("1");
    gtk_fixed_put(GTK_FIXED(fixedC), botao1, 10, 25);
    gtk_widget_set_size_request(botao1, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao1), "clicked", GTK_SIGNAL_FUNC(concat), "1");

    botao2 = gtk_button_new_with_label("2");
    gtk_fixed_put(GTK_FIXED(fixedC), botao2, 50, 25);
    gtk_widget_set_size_request(botao2, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao2), "clicked", GTK_SIGNAL_FUNC(concat), "2");

    botao3 = gtk_button_new_with_label("3");
    gtk_fixed_put(GTK_FIXED(fixedC), botao3, 90, 25);
    gtk_widget_set_size_request(botao3, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao3), "clicked", GTK_SIGNAL_FUNC(concat), "3");

    botao4 = gtk_button_new_with_label("4");
    gtk_fixed_put(GTK_FIXED(fixedC), botao4, 10, 65);
    gtk_widget_set_size_request(botao4, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao4), "clicked", GTK_SIGNAL_FUNC(concat), "4");

    botao5 = gtk_button_new_with_label("5");
    gtk_fixed_put(GTK_FIXED(fixedC), botao5, 50, 65);
    gtk_widget_set_size_request(botao5, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao5), "clicked", GTK_SIGNAL_FUNC(concat), "5");

    botao6 = gtk_button_new_with_label("6");
    gtk_fixed_put(GTK_FIXED(fixedC), botao6, 90, 65);
    gtk_widget_set_size_request(botao6, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao6), "clicked", GTK_SIGNAL_FUNC(concat), "6");

    botao7 = gtk_button_new_with_label("7");
    gtk_fixed_put(GTK_FIXED(fixedC), botao7, 10, 105);
    gtk_widget_set_size_request(botao7, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao7), "clicked", GTK_SIGNAL_FUNC(concat), "7");

    botao8 = gtk_button_new_with_label("8");
    gtk_fixed_put(GTK_FIXED(fixedC), botao8, 50, 105);
    gtk_widget_set_size_request(botao8, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao8), "clicked", GTK_SIGNAL_FUNC(concat), "8");

    botao9 = gtk_button_new_with_label("9");
    gtk_fixed_put(GTK_FIXED(fixedC), botao9, 90, 105);
    gtk_widget_set_size_request(botao9, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao9), "clicked", GTK_SIGNAL_FUNC(concat), "9");

    botao0 = gtk_button_new_with_label("0");
    gtk_fixed_put(GTK_FIXED(fixedC), botao0, 50, 145);
    gtk_widget_set_size_request(botao0, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botao0), "clicked", GTK_SIGNAL_FUNC(concat), "0");

    //botao das operacoes
    botaoIgual = gtk_button_new_with_label("=");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoIgual, 90, 145);
    gtk_widget_set_size_request(botaoIgual, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoIgual), "clicked", GTK_SIGNAL_FUNC(Resultado), NULL);

    botaoC = gtk_button_new_with_label("C");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoC, 10, 145);
    gtk_widget_set_size_request(botaoC, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoC), "clicked", GTK_SIGNAL_FUNC(LimparT), NULL); //limpeza total

    botaoSoma = gtk_button_new_with_label("+");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoSoma, 130, 25);
    gtk_widget_set_size_request(botaoSoma, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoSoma), "clicked", GTK_SIGNAL_FUNC(Operacao), "Soma");

    botaoSub = gtk_button_new_with_label("-");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoSub, 130, 65);
    gtk_widget_set_size_request(botaoSub, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoSub), "clicked", GTK_SIGNAL_FUNC(Operacao), "Subtracao");

    botaoDiv = gtk_button_new_with_label("/");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoDiv, 130, 105);
    gtk_widget_set_size_request(botaoDiv, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoDiv), "clicked", GTK_SIGNAL_FUNC(Operacao), "Divisao");

    botaoMult = gtk_button_new_with_label("*");
    gtk_fixed_put(GTK_FIXED(fixedC), botaoMult, 130, 145);
    gtk_widget_set_size_request(botaoMult, 40, 40);
    gtk_signal_connect(GTK_OBJECT(botaoMult), "clicked", GTK_SIGNAL_FUNC(Operacao), "Multiplicacao");

    gtk_widget_show_all(janela);

    gtk_main();

    return 0;
}
