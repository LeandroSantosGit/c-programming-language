#include<stdio.h>

void main(){

    int x[] = {10,20};

    //imprimir enderecos
    printf("Endereco:              %p\n", x);
    printf("Endereco da Soma:      %p\n", x + 1);
    printf("Endereco da Subtracao: %p\n", ((x + 1) -1));

    //imprimir as operacoes aritmeticas
    printf("Valor da varialvel:    %i\n", *x);
    printf("Soma da variavel:      %i\n", *(x + 1));
    printf("Subtracao da variavel: %i\n", *((x + 1) -1));
}
