#include<stdio.h>

void main(){

    int contagem = 0;

    while(contagem < 100){
        contagem++;
        printf("Numero: %i\n", contagem);

        if(contagem == 10){
            printf("Deste ponto estamos continuando: %i\n", contagem);
            continue;
        }else if(contagem == 30){
            printf("Neste ponto estamos parando: %i\n", contagem);
            break;
        }
    }
}
