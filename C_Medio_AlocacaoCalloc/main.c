#include <stdio.h>
#include <stdlib.h>

int main(){

    int quantidadeE;
    int i;
    int *vetor;

    printf("Digite a quantidade de valores do vetor: \n");
    scanf("%d", &quantidadeE);

    vetor = calloc(quantidadeE, sizeof(int));

    if(!vetor){
        printf("Memoria esgotada!");
        exit(1);
    }

    printf("Digite os valores para vetor: \n");
    for(i = 0; i < quantidadeE; i++){
        printf("Valor para vetor: %d\n", i);
        scanf("%d", &vetor[i]);
    }

    printf("Valores do vetor: \n");
    for(i = 0; i < quantidadeE; i++){
        printf("Vetor[%d] - %d\n", i, vetor[i]);
    }
    free(vetor);

    return 0;
}
