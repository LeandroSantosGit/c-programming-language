#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

//Variaveis globais

GtkWidget *janela;
GtkWidget *fixedC;
GtkWidget *botao1, *botao2;
GtkWidget *rotuloNome, *rotuloIdade, *rotuloProfissao, *rotuloEmpresa;
GtkWidget *rotuloVal1, *rotuloVal2, *rotuloVal3, *rotuloVal4;
GtkWidget *cxNome, *cxIdade, *cxProfissao, *cxEmpresa;

char *nome;
int idade;
char *profissao;
char *empresa;

void Cadastro(){

    nome = gtk_entry_get_text(cxNome); //atribuir os dados digitados pelo usuario as variaveis
    idade = gtk_entry_get_text(cxIdade);
    profissao = gtk_entry_get_text(cxProfissao);
    empresa = gtk_entry_get_text(cxEmpresa);

    gtk_label_set_text(rotuloVal1, nome); //adcionar o valor das variaveis aos rotulos
    gtk_label_set_text(rotuloVal2, idade);
    gtk_label_set_text(rotuloVal3, profissao);
    gtk_label_set_text(rotuloVal4, empresa);
}

void Limpar(){

    gtk_entry_set_text(cxNome, ""); //limpar as caixas de texto
    gtk_entry_set_text(cxIdade, "");
    gtk_entry_set_text(cxProfissao, "");
    gtk_entry_set_text(cxEmpresa, "");
}

void Fechar(){
 //fechr
    gtk_main_quit();
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Cadastro");
    gtk_window_set_default_size(GTK_WINDOW(janela), 450, 200);
    gtk_window_set_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    fixedC = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(janela), fixedC);

    //rotulos

    rotuloNome = gtk_label_new("Nome");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloNome, 20, 30);

    rotuloIdade = gtk_label_new("Idade");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloIdade, 20, 60);

    rotuloProfissao = gtk_label_new("Profissao");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloProfissao, 20, 90);

    rotuloEmpresa = gtk_label_new("Empresa");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloEmpresa, 20, 120);

    //rotulos de resposta

    rotuloVal1 = gtk_label_new("Nome");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloVal1, 280, 30);

    rotuloVal2 = gtk_label_new("Idade");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloVal2, 280, 60);

    rotuloVal3 = gtk_label_new("Profissao");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloVal3, 280, 90);

    rotuloVal4 = gtk_label_new("Empresa");
    gtk_fixed_put(GTK_FIXED(fixedC), rotuloVal4, 280, 120);

    // caixas de texto

    cxNome = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), cxNome, 80, 28);

    cxIdade = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), cxIdade, 80, 58);

    cxProfissao = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), cxProfissao, 80, 88);

    cxEmpresa = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), cxEmpresa, 80, 118);

    //botoes

    botao1 = gtk_button_new_with_label("Cadastrar");
    gtk_fixed_put(GTK_FIXED(fixedC), botao1, 20, 150);
    gtk_widget_set_size_request(botao1, 80, 30);
    gtk_signal_connect(GTK_OBJECT(botao1), "clicked", GTK_SIGNAL_FUNC(Cadastro), NULL);

    botao2 = gtk_button_new_with_label("Limpar");
    gtk_fixed_put(GTK_FIXED(fixedC), botao2, 160, 150);
    gtk_widget_set_size_request(botao2, 80, 30);
    gtk_signal_connect(GTK_OBJECT(botao2), "clicked", GTK_SIGNAL_FUNC(Limpar), NULL);

    gtk_widget_show_all(janela);

    gtk_main();

    return 0;
}
