#include <stdio.h>
#include <stdlib.h>

struct NO{

    char letra;
    struct NO *proximo;
};

typedef struct NO no;  //no substitui struct NO pra ficar mais facil o desenvolvimento

no *RETIRADA(no *FILA){

    if(FILA->proximo == NULL){ //veirificar se fila esta vazia
        printf("Fila vazia!");
    }else{          //retirar o proximo ou seja o primeiro elemento da fila
        no *temp = FILA->proximo;
        FILA->proximo = temp->proximo;
    }
}

void OPCOES(no *FILA, int op){

    no *temp;  //ponteiro temporario pra auxiliar na chamada da funcao retirada
    switch(op){
        case 0:
            LIBERACAO(FILA);
            break;
        case 1:
            INSERIR(FILA);
            break;
        case 2:
            temp = RETIRADA(FILA);
            break;
        case 3:
            EXIBIR(FILA);
            break;
        default:
            printf("Comando Invalido!");

    }
}

int Vazia(no *FILA){

    if(FILA->proximo == NULL){ //verificar se fila esta vazia
        return 1;  //se estiver vazia retorna 1
    }else{
        return 0;
    }
}

no *ALOCACAO(){

    no *novo = malloc(sizeof(no)); //alocar na memoria

    if(!novo){  //verificar se foi alocado na memoria
        printf("Sem memoria disponivel!");
        exit(1);
    }else{
        printf("Novo elemento na fila: "); //usuario digitar elemento para alocar
        scanf("%s", &novo->letra);
        return novo;
    }
}

void INSERIR(no *FILA){

    no *novo = ALOCACAO(); //chama funcao alocacao pra alocar novo elemento
    novo->proximo = NULL;  //novo tem que ser inserino em null que e o ultimo da fila

    if(Vazia(FILA)){
        FILA->proximo = novo; //se fila estiver vazia alocar novo elemento
    }else{
        no *temp = FILA->proximo;

        while(temp->proximo != NULL)  //procurar ultimo da fila
            temp = temp->proximo;

            temp->proximo = novo;  //acolacar em ultimo lugar
    }

}

void EXIBIR(no *FILA){

    if(Vazia(FILA)){ // verificar se fila esta vazia
        printf("Fila esta vazia!\n\n");
        return ;
    }

    no *temp;   //ponteiro temporaria
    temp = FILA->proximo;
    printf("\nFila: ");
    while(temp != NULL){  //laco pra imprimir ate chagar no ultimo elemento
        printf("%c", temp->letra);
        temp = temp->proximo;
    }
    printf("\n\n");
}

void LIBERACAO(no *FILA){  //funcao pra liberar memoria quando sair da aplicacao

    if(Vazia(FILA)){ //virificar se fila esta vazia

        no *proxNode,  //ponteiros temporarios
            *atual;

        atual = FILA->proximo;
        while(atual != NULL){  //liberar ate o ultimo elemento
            proxNode = atual->proximo;
            free(atual);  //liberar autal
            atual = proxNode;  //liberar proNode
        }
    }
}

int main()
{
    int opt; //varialvel que sao as opcoes que o usuario ira escolher

    no *FILA = malloc(sizeof(no));  //ponteiro pra efetuar a alocacao

    if(!FILA){  //verifica se e possivel fazer alocacao
        printf("Sem memoria!");
        exit(1);
    }else{  //se nao apresenta proximo da fila e imprime opcoes
        FILA->proximo = NULL;

        do{
            printf("Opcoes\n");
            printf("0 - Sair\n");
            printf("1 - Inserir\n");
            printf("2 - Retirar\n");
            printf("3 - Exibir\n");
            printf("Opcao: ");
            scanf("%d", &opt);

            OPCOES(FILA, opt);
        }while(opt);

        free(FILA);
        return 0;
    }
}
