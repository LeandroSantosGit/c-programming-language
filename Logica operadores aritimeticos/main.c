#include <stdio.h>
#include <stdlib.h>

int main()
{
    int numero1;
    int numero2;

    printf("Digite o primeiro numero: ");
    scanf("%d", &numero1);
    printf("Digite o segundo numero: ");
    scanf("%d", &numero2);

    printf("Adicao: %d + %d = %d\n", numero1, numero2, numero1 + numero2);
    printf("Subtracao: %d - %d = %d\n", numero1, numero2, numero1 - numero2);
    printf("Multiplicacao: %d * %d = %d\n", numero1, numero2, numero1 * numero2);
    printf("Divisao: %d / %d = %d\n", numero1, numero2, numero1/numero2);
    printf("O resto de %d dividido pro %d e %d", numero1, numero2, numero1%numero2);
    return 0;
}
