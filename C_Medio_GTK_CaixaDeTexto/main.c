#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void Deleta(){ //funcao mostra no compilador toda vez que algo for apagado

    g_print("deletou\n");
}

void Fechar(){

    gtk_main_quit();
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *caxTexto; //ponteiro para caixa de texto


    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo");
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    caxTexto = gtk_entry_new();  //criar a caixa de texto
    gtk_container_add(GTK_CONTAINER(janela), caxTexto); //adicionar caixa de texto na janela
    gtk_signal_connect(GTK_OBJECT(caxTexto), "delete-text", GTK_SIGNAL_FUNC(Deleta), NULL); //recebe o sinal de
                                                                                            // deletar do usuario
    gtk_widget_show(janela);
    gtk_widget_show(caxTexto);

    gtk_main();

    return 0;
}
