#include <stdio.h>
#include <stdlib.h>
#include <time.h>


/* Asctime */ //funcao para converter struct para string em formato de dias horas ano etc
/*
int main()
{
    struct tm t;
        t.tm_sec  = 30; //segundos
        t.tm_min  = 20; //minutos
        t.tm_hour = 8;  //horas
        t.tm_mday = 20; //dia
        t.tm_mon  = 2;  //mes
        t.tm_year = 94; //ano
        t.tm_wday = 6;  //semana

        puts(asctime(&t));

    return 0;
} */


/* Ctime */ //converter para string a data e hora atual
/*
int main(){

    time_t tempo;

    time(&tempo);

    printf("%s\n\n", ctime(&tempo));

    return 0;
} */


/* Difftime */ //retorna em segundos a diferenca entre dois espaco de tempo

#include <windows.h>
#define sleep(x) Sleep(1000 * x)

int main(){

    time_t inicio_tempo, fim_tempo;
    double diff;

    printf("Iniciando......\n");
    time(&inicio_tempo);

    sleep(5); //paralizar execucao por segundos determinado

    time(&fim_tempo);
    diff = difftime(fim_tempo, inicio_tempo); //verificar a diferenca do espaco de tempo

    printf("Time = %f\n", diff);

    return 0;
}
