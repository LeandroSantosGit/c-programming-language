#include <stdlib.h>
#include <gtk/gtk.h>

GtkBuilder *builder;
GtkWidget *window;
GtkWidget *toggle1; //ira receber os radios button
GtkWidget *toggle2;

G_MODULE_EXPORT void Seleciona1(gpointer t){ //verificar qual radio button esta selecionado e imprimir no console

    if(gtk_toggle_button_get_active(t) == 1){
        g_print("C++\n");
    }
}

G_MODULE_EXPORT void Seleciona2(gpointer t){

    if(gtk_toggle_button_get_active(t) == 1){
        g_print("C\n");
    }
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    toggle1 = GTK_WIDGET(gtk_builder_get_object(builder, "rb1")); //passar os radio button para as variaveis
    toggle2 = GTK_WIDGET(gtk_builder_get_object(builder, "rb2"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show_all(window);

    gtk_main();

    return (0);
}
