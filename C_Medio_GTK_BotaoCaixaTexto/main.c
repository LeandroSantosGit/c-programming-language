#include <stdlib.h>
#include <gtk/gtk.h>

//ponteiros globais
GtkWidget *janela;
GtkWidget *fixedC;
GtkWidget *botao1;
GtkWidget *botao2;
GtkWidget *rotulo1;
GtkWidget *caixTxt;

void Fechar(){ //fechar o aplicativo

    gtk_main_quit();
}

void Valor(){ //adicionar texto no rotulo1

    gtk_label_set_text(rotulo1, "Aperte o botao 1");
}

void Valor2(){ //adcionar texto na rotulo 2 ou seja caixa de texto

    gtk_entry_set_text(caixTxt, "Aperte o botao 2");
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "TreinaWeb");
    gtk_window_set_default_size(GTK_WINDOW(janela), 400, 300);
    gtk_window_set_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    fixedC = gtk_fixed_new();
    gtk_container_add(GTK_CONTAINER(janela), fixedC);

    botao1 = gtk_button_new_with_label("Botao1");
    gtk_fixed_put(GTK_FIXED(fixedC), botao1, 0, 0);
    gtk_widget_set_size_request(botao1, 80, 30);
    gtk_signal_connect(GTK_OBJECT(botao1), "clicked", GTK_SIGNAL_FUNC(Valor), NULL); //quando o botao for clicado ira exibir valor

    botao2 = gtk_button_new_with_label("Botao2");
    gtk_fixed_put(GTK_FIXED(fixedC), botao2, 0, 50);
    gtk_widget_set_size_request(botao2, 80, 30);
    gtk_signal_connect(GTK_OBJECT(botao2), "clicked", GTK_SIGNAL_FUNC(Valor2), NULL);
                                                                            //quando o botao for clicado ira exibir valor
    rotulo1 = gtk_label_new("Oi");
    gtk_fixed_put(GTK_FIXED(fixedC), rotulo1, 100, 0);

    caixTxt = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(fixedC), caixTxt, 100, 50);

    gtk_widget_show_all(janela);

    gtk_main();

    return 0;
}
