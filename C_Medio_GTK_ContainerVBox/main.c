#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

//VBox container na vertical

void Fechar(){

    gtk_main_quit();
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *Vbox;
    GtkWidget *botao1;
    GtkWidget *botao2;
    GtkWidget *botao3;

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Leandro");
    gtk_window_set_default_size(GTK_WINDOW(janela), 800,600);
    gtk_window_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    Vbox = gtk_vbox_new(TRUE, 1); //TRUE todos widgets dentro do vbox teram mesmo tamanho, e 1 e o espacamento entre widgets
    gtk_container_add(GTK_CONTAINER(janela), Vbox); //adcionar vbox na janela
     //adcionar botoes no vbox
    botao1 = gtk_button_new_with_label("Botao1");

    botao2 = gtk_button_new_with_label("Botao2");

    botao3 = gtk_button_new_with_label("Botao3");

    gtk_box_pack_start(GTK_BOX(Vbox), botao1, TRUE, TRUE, 0);7
    //gtk_box_pack_start(GTK_BOX(Vbox), botao1, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(Vbox), botao2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(Vbox), botao3, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(Vbox), botao3, TRUE, FALSE, 0);

    gtk_widget_show_all(janela);

    gtk_main();

    return 0;
}
