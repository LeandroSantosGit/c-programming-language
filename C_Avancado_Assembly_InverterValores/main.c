#include <stdio.h>
#include <stdlib.h>

//mover valor da variavel numero1 para numero2

int main()
{
    int numero1 = 10, numero2;
                                             //output          input
    asm("movl %1, %%ebx;" "movl %%ebx, %0;" : "=r"(numero2) : "r"(numero1));

    printf("Numero2: %d\n", numero2);

    return 0;
}
