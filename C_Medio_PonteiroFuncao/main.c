#include <stdio.h>
#include <stdlib.h>

float maior(float x, float y){

    return ((x > y)? x : y);
}

float menor(float x, float y){

    return (x < y? x : y);
}

float resultado(float x, float y, float (*funcao)(float, float)){

    return (funcao(x,y));
}

int main(){

    float resul;

    resul = resultado(7, 6, &maior);

    printf("O maior numero e: %.2f\n", resul);

    return 0;
}
