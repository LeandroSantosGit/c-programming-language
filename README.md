## C L�gica de Programa��o

* Ter os primeiros contatos com uma linguagem de programa��o "de verdade": o C;
* Entender� conceitos de entrada e sa�da de dados do C, conceitos estes utilizados por quase todas as outras linguagens;
* Compreender algoritmos um pouco mais complexos, como o algoritmo de ordena��o de dados chamado Bubble Sort;
* Implementar conceitos matem�ticos com linguagens de programa��o, como as famosas sequ�ncias de Fibonacci.

## C B�sico

No curso foi abordado os conceitos b�sicos da linguagem: tipo de dados, vari�veis, escopo; As estruturas condicionais e repeti��o; Como trabalhar com fun��es; Como funcionam os ponteiros para programar na linguagem C.

* Processo de compila��o
* Tipos de dados e identificados
* Vari�veis
* Operadores
* Condicionais
* La�os
* Matrizes
* Estruturas
* Fun��es
* Ponteiros
* Entrada e Sa�da
* Pr�-processadores
* Filas e Pilhas
* Filas e Pilhas

## C Intermedi�rio

No curso foi abordado os principais conceitos para acesso a Banco de Dados; Implementa��o de estruturas de dados, como listas, filas, pilhas e �rvores;  Funcionamento da aloca��o din�mica; Criar interfaces gr�ficas com GTK, e outros recursos da linguagem C.

* Ponteiros
* Aloca��o din�mica e est�tica de mem�ria
* Struct
* Union
* Enum
* Typedef
* Filas e Pilhas
* Interface Gr�fica - GTK
* Banco de Dados - MySQL
* OpenGL Glut
* Sockets

# C Avan�ado

No curso foi abordado conceitos para criar e exportar e importar DLLs; Adicionar c�digos Assembly junto com o C++; Criar Sockets e Implementar streams, al�m de outros recursos mais avan�ados da linguagem.

* Bibliotecas - stdlib.h, string.h, math.h e time.h
* CGI
* GTK
* Glade
* Banco de Dados - MySQL
* DLL
* Sockets






