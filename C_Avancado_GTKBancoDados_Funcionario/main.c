#include <stdlib.h>
#include <gtk/gtk.h>
#include <stdio.h>
#include <mysql.h>

//Variaveis globais

MYSQL conn;  //coneaxao com banco de dados
GtkWidget *clist01;   //apresentar os dados na tabela
GtkWidget *cxNome, *cxSobrenome;
char *titulos[3] = {"ID", "Nome   ", "Sobrenome"}; //titulos das colunas da tabela
char query[100];
char *texto;  //auxilia nas acoes que serao executados baseando pelo id
int res;   //auxilia nos resultados das intrusoes como inserir, atualizar e excluir

void limpezaClist(){ //funcao para limpar o clist e apresentar apenas oque o usuario deseja

    gtk_clist_clear((GtkCList *) clist01);
}

void limpezaCxs(){ //funcao para limpar as caixas de texto

    gtk_entry_set_text(cxNome,"");
    gtk_entry_set_text(cxSobrenome,"");
}

void Seleciona(GtkWidget *clist, gint row, gint column, GdkEventButton *event, gpointer data){ //variaveis para pegar valores de
    // Obtem-se o texto armazenado na celula cliclada.                                        //  uma celula contida dentro de
    gtk_clist_get_text(GTK_CLIST(clist01), row, column, &texto);                              //uma linha e uma coluna
    // Mostra no console informacoes sobre a linha selecionada
    g_print("Voce selecionou a linha %d. Voce selecionou na coluna %d, e o texto nesta celula e %s\n\n", row, column, texto);

    return;
}

void pesquisa(gpointer data){

    limpezaClist(); //para garantir que o clist esteja vazio na hora de listar os dados

    mysql_init(&conn);  //iniciar mysql

    if(mysql_real_connect(&conn, "localhost", "root", "", "funcionarios", 0, NULL, 0)){ //conectar ao bando de dados

        mysql_query(&conn, "SELECT * FROM listas;"); //faz uma busca

        MYSQL_RES *result = mysql_store_result(&conn); //recuperar os dados da pesquisa

        if(result == NULL){ //verificar se foi possivel obter resultado da pesquisa
            fprintf(stderr, "%s\n", mysql_error(&conn));
            mysql_close(&conn);
            exit(1);
        }

        int num_fields = mysql_num_fields(result)-2; //receber dados e exbir de forma correta

        MYSQL_ROW row; //exibir os valores dentro do clist
        int i; //controlar laco for

        while((row = mysql_fetch_row(result))){ //laco para ser executado enquato esxitir valores
            printf("\n");
            for(i = 0; i < num_fields; i++){ //laco para imprimir valores
                g_print("\n%s ", row[i] ? row[i] : "NULL");  //imprimir no console
                gtk_clist_append((GtkCList *) data, &row[i]); //adicionar os valores dentro de clist
            }
            printf("\n");
        }

        mysql_free_result(result); //libera a memoria
        mysql_close(&conn);  //fecha a conexao
    }else{
        printf("Erro Conexao!"); //se nao obter conexao com bando de dados informar erro ao usuario
    }
}

void inserir(){

    char *pegaNome = gtk_entry_get_text(cxNome); //variaveis para armazenar valores digitado na caixa de texto
    char *pegaSobrenome = gtk_entry_get_text(cxSobrenome);

    mysql_init(&conn);  //iniciar mysql

    if(mysql_real_connect(&conn, "localhost", "root", "", "funcionarios", 0, NULL, 0)){ //conecyar ao banco de dados
        sprintf(query, "INSERT INTO listas(nome, sobrenome) VALUES('%s', '%s');", pegaNome, pegaSobrenome); //padar os valores
        res = mysql_query(&conn, query); //atribir a variavel o valor do query obitido acima                  para as variaveis

        if(!res){ //verificar se foi possivel inserir ou nao os registros e imformar no console
            printf("Resgistros inseridos %d\n", mysql_affected_rows(&conn));
        }else{
            printf("Erro na insercao %d : %s\n", mysql_errno(&conn), mysql_error(&conn));
        }

        mysql_close(&conn); //fechar conexao
        limpezaClist(); //limpar clist
        limpezaCxs(); //limpar caixa de texto

    }else{ //imformar ao usuario se nao obter conexao com banco de dados
        printf("Erro Conexao!");
    }
}

void pesquisaM(gpointer data){

    char *nomeBusca = gtk_entry_get_text(cxNome); //variavel para armazenar os dados digitados pelo usuario

    limpezaClist();

    mysql_init(&conn);

    if(mysql_real_connect(&conn, "localhost", "root", "", "funcionarios", 0, NULL, 0)){
        sprintf(query, "SELECT * FROM listas WHERE nome LIKE '%%%s%%';", nomeBusca); //LIKE ultizado pra que o usuario digite
        mysql_query(&conn, query);                                                  // apenas um pedaco da palavra e ja retorne
                                                                                    // o valor da pesquisa
        MYSQL_RES *result = mysql_store_result(&conn);

        if(result == NULL){
            fprintf(stderr, "%s\n", mysql_error(&conn));
            mysql_close(&conn);
            exit(1);
        }

        int num_fields = mysql_num_fields(result)-2;

        MYSQL_ROW row;
        int i;

        while((row = mysql_fetch_row(result))){
            printf("\n");
            for(i = 0; i < num_fields; i++){
                gtk_clist_append((GtkCList *) data, &row[i]);
            }
            printf("\n");
        }

        mysql_free_result(result);
        mysql_close(&conn);

    }else{
        printf("Erro na Conexao!");
    }
}

void update(){

    char *valorNome = gtk_entry_get_text(cxNome); //variaveis para pegar os valores das caixas de texto
    char *valorSobrenome = gtk_entry_get_text(cxSobrenome);

    mysql_init(&conn);

    if(mysql_real_connect(&conn, "localhost", "root", "", "funcionarios", 0, NULL, 0)){ //atulizar valores no bando de dados
        sprintf(query, "UPDATE listas SET nome = '%s', sobrenome = '%s' WHERE id = '%s';", valorNome, valorSobrenome, texto);
        res = mysql_query(&conn, query);

        if(!res){ //se forem ou nao atulizados imformar no console
            printf("Resgistros atualizados %d\n", mysql_affected_rows(&conn));
        }else{
            printf("\nErro na atualizacao %d : %s\n", mysql_errno(&conn), mysql_error(&conn));
        }

        mysql_close(&conn);
        limpezaClist();
        limpezaCxs();

    }else{
        printf("Erro na Conexao!");
    }
}

void deletar(){

    mysql_init(&conn);

    if(mysql_real_connect(&conn, "localhost", "root", "", "funcionarios", 0, NULL, 0)){
        sprintf(query, "DELETE FROM listas WHERE id = '%s';", texto); //exclir valors no banco de dados
        res = mysql_query(&conn, query);

        if(!res){
            printf("Registros excluidos %d\n", mysql_affected_rows(&conn));
        }else{
            printf("Erro na exclusao %d : %s\n", mysql_errno(&conn), mysql_error(&conn));
        }

        mysql_close(&conn);
        limpezaClist();
        limpezaCxs();

    }else{
        printf("Erro na Conexao!");
    }
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *container;
    GtkWidget *rotuloNome, *rotuloSobrenome;
    GtkWidget *botaoCadastrar, *botaoSelecionar, *botaoAtualizar, *botaoExcluir, *botaoListar;

    gtk_init(&argc, &argv); //iniciar GTK

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL); //criar janela
    gtk_window_set_title(GTK_WINDOW(janela), "Funcionarios"); //adcionar nome a janela
    container = gtk_fixed_new();  //criar o container
    gtk_container_add(GTK_CONTAINER(janela), container); //adcionar o container na janela

    //Clist ira apresentar os dados na janela

    clist01 = gtk_clist_new_with_titles(3, titulos); //iniciar clist com os titulos definidos pela variavel clist01
    gtk_clist_set_shadow_type(GTK_CLIST(clist01), GTK_SHADOW_OUT); //definir tipo de sombra pro clist
    gtk_clist_set_column_width(GTK_CLIST(clist01), 0, 350); //definir largura da coluna
    gtk_fixed_put(GTK_FIXED(container), clist01, 0, 0); //fixar no container
    gtk_signal_connect(GTK_OBJECT(clist01), "select_row", GTK_SIGNAL_FUNC(Seleciona), NULL); //adicionar o sinal e a sua funcao

    //rotulos

    rotuloNome = gtk_label_new("Nome");
    gtk_fixed_put(GTK_FIXED(container), rotuloNome, 0, 234);

    rotuloSobrenome = gtk_label_new("Sobrenome");
    gtk_fixed_put(GTK_FIXED(container), rotuloSobrenome, 0, 286);

    //caixa texto

    cxNome = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(container), cxNome, 80, 234);

    cxSobrenome = gtk_entry_new();
    gtk_fixed_put(GTK_FIXED(container), cxSobrenome, 80, 284);

    //botoes iniciar botoes, fixar no container, adicionar o sinal e suas funcoes

    botaoListar = gtk_button_new_with_label("Listar");
    gtk_fixed_put(GTK_FIXED(container), botaoListar, 300, 320);
    gtk_signal_connect_object(GTK_OBJECT(botaoListar), "clicked", GTK_SIGNAL_FUNC(pesquisa), (gpointer) clist01);

    botaoCadastrar = gtk_button_new_with_label("Cadastrar");
    gtk_fixed_put(GTK_FIXED(container), botaoCadastrar, 20, 320);
    gtk_signal_connect_object(GTK_OBJECT(botaoCadastrar), "clicked", GTK_SIGNAL_FUNC(inserir), NULL);

    botaoSelecionar = gtk_button_new_with_label("Selecionar");
    gtk_fixed_put(GTK_FIXED(container), botaoSelecionar, 70, 320);
    gtk_signal_connect_object(GTK_OBJECT(botaoSelecionar), "clicked", GTK_SIGNAL_FUNC(pesquisaM), (gpointer) clist01);

    botaoAtualizar = gtk_button_new_with_label("Atualizar");
    gtk_fixed_put(GTK_FIXED(container), botaoAtualizar, 140, 320);
    gtk_signal_connect_object(GTK_OBJECT(botaoAtualizar), "clicked", GTK_SIGNAL_FUNC(update), NULL);

    botaoExcluir = gtk_button_new_with_label("Excluir");
    gtk_fixed_put(GTK_FIXED(container), botaoExcluir, 200, 320);
    gtk_signal_connect_object(GTK_OBJECT(botaoExcluir), "clicked", GTK_SIGNAL_FUNC(deletar), NULL);

    gtk_widget_show_all(janela);
    gtk_main();

    return 0;
}
