#include <stdio.h>
#include <stdlib.h>

//Matriz com estrutura

    struct Cao {

        char *nome;
        int idade;
        char *raca;
    };

int main()
{
    struct Cao dog[3][3]; //matriz 3 linhas e 3 colunas
    int x; //linha
    int y; //coluna

    dog[0][0].nome = "Juju";
    dog[0][0].idade = 5;
    dog[0][0].raca = "Pixem";

    dog[0][1].nome = "Biju";
    dog[0][1].idade = 10;
    dog[0][1].raca = "Pixem";

    dog[0][2].nome = "Zeze";
    dog[0][2].idade = 3;
    dog[0][2].raca = "Pixem";

    dog[1][0].nome = "Valente";
    dog[1][0].idade = 8;
    dog[1][0].raca = "Pitbull";

    dog[1][1].nome = "Monstro";
    dog[1][1].idade = 10;
    dog[1][1].raca = "Pitbull";

    dog[1][2].nome = "Negao";
    dog[1][2].idade = 4;
    dog[1][2].raca = "Pitbull";

    dog[2][0].nome = "Amarelo";
    dog[2][0].idade = 3;
    dog[2][0].raca = "Vira Lata";

    dog[2][1].nome = "Negao";
    dog[2][1].idade = 3;
    dog[2][1].raca = "Vira Lata";

    dog[2][2].nome = "Bobo";
    dog[2][2].idade = 4;
    dog[2][2].raca = "Vira Lata";

    for(x = 0; x < 3; x++){
        printf("Cao %d\n", x+1);
        for(y = 0; y < 3; y++){
            printf("Nome: %s   Idade: %d   Raca: %s\n", dog[x][y].nome, dog[x][y].idade, dog[x][y].raca);
        }
    }

    return 0;
}
