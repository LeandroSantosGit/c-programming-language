#include <stdio.h>
#include <stdlib.h>

int main()
{
    int vetor[5];
    int i;
    int numero;
    int alternacoes = 0;

    for(i = 0; i < 5; i++) {
            printf("Digite um numero para entrar no vetor: ");
            scanf("%d", &numero);
            vetor[i] = numero;
    }
    printf("Vetor antes da ordenacao: \n");
    for(i = 0; i < 5; i++){
        numero = vetor[i];
        printf("%d\n", numero);
    }

    alternacoes = 1;
    int temporario = 0;
    while(alternacoes > 0) {
            alternacoes = 0;
        for(i = 0; i < 4; i++) {
            if(vetor[i] > vetor[i + 1]) {
                temporario = vetor[i];
                vetor[i] = vetor[i + 1];
                vetor[i + 1] = temporario;
                alternacoes = alternacoes + 1;
            }
        }
    }
    printf("Resultado da ordenacao: \n");
    for(i = 0; i < 5; i++) {
        numero = vetor[i];
        printf("%d\n", numero);
    }
    return 0;
}
