#include <stdio.h>
#include <stdlib.h>

int main()
{
    int a, b , c;

    printf("Informe lado A: ");
    scanf("%d", &a);
    printf("Informe lado B: ");
    scanf("%d", &b);
    printf("Informe lado C: ");
    scanf("%d", &c);

    if((a == b) && (b == a)) {
        printf("O triangulo e equilatero");
    }else if ((a != b) && (b != c) && (a !=c)){
        printf("O triangulo e escaleno");
    }else {
        printf("O triangulo e isoceles");
    }
    return 0;
}
