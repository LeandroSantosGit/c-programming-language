#include <stdio.h>
#include <stdlib.h>

union Numeros{

    float numero1;
    int numero2;
};

int main()
{
    union Numeros p1;
    int op;

    scanf("%d", &op);

    if(op == 1){
        p1.numero1 = 4.5;
        printf("Numero1: %f\n", p1.numero1);
    }else{
        p1.numero2 = 2;
        printf("Numero2: %d\n", p1.numero2);
    }

    return 0;
}
