#include <stdio.h>
#include <stdlib.h>

/* Atof */ //converte string para float
/*
int main()
{
    char *numero = "10.5";
    float num1;
    float num2 = 2.5;
    float resultado;

    num1 = atof(numero); //convertendo string para float
    resultado = num2 + num1;

    printf("%f\n", resultado);

    return 0;
} */


/* Atoi */ //converter string para inteiro(int)
/*
int main(){

    char *numero = "10";
    int num1;
    int num2 = 2;
    int resultado;

    num1 = atoi(numero); //convertendo string para inteiro
    resultado = num1 + num2;

    printf("%d\n", resultado);

    return 0;
} */


/* Atol */ //converter string para long interger
/*
int main(){

    char *numero = "100000";
    long num1;
    long num2 = 20000;
    long resultado;

    num1 = atol(numero); //convertendo string para long interger
    resultado = num2 + num1;

    printf("%ld\n", resultado);

    return 0;
} */



/* Strtod */ //converter string para double
/*
int main(){

    char *s = "20.30300";
    char *ptr;
    double resultado;

    resultado = strtod(s, &ptr); //convertendo string passando o valor para uma variavel double

    printf("%lf\n", resultado);

    return 0;
} */


/* Rand */ //gera volor pseudo aleatorio
/*
int main(){

    int i;

    printf("Gerando 5 valores aleatorios:\n\n");

    for(i = 0; i < 5; i++){ //gerar 5 valores aleatorios de 0 a 100
        printf("%d ", rand() % 100);
    }

    return 0;
} */


/* Abort */ //forca o termino da execucao
/*
int main(){

    int i;

    for(i = 0; i < 5; i++){ //criar um laco
        if(i == 2){ //condicao para abortar a execucao no numero determinado
            abort();
        }
        printf("%d ", i);
    }
    return 0;
} */


/* Exit */ //termina em imediato a execusao
/*
int main(){

    int i;

    for(i = 0; i < 5; i++){ //criar um laco
        if(i == 2){  //condicao para finalizar execucao no numero determinado
            exit(0);
        }
        printf("%d ", i);
    }

    return 0;
} */


/* System */ //executa comando externo
/*
int main(){

    system("ipconfig");

    return 0;
} */


/* Abs */ //valor absoluto
/*
int main(){

    int x, y;

    x = abs(10);
    y = abs(-5);

    printf("%d || %d\n", x, y);

    return 0;
} */


/* Div */ //funcao de divisao inteira
/*
int main(){

    div_t resultado;

    resultado = div(11, 2); //valores para ser divididos

    printf("Quociente: %d\n\n", resultado.quot); //exibir o quociente

    printf("Resto: %d\n\n", resultado.rem); //exibir o resto

    return 0;
} */
