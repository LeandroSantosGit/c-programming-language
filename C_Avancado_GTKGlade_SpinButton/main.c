#include <stdlib.h>
#include <gtk/gtk.h>

GtkBuilder *builder;
GtkWidget *window;
GtkWidget *spin; //varialvel do spinbutton

G_MODULE_EXPORT void pegaValor(gpointer t){ //funcao para recebe os valores so spinbutton

    int valor = gtk_spin_button_get_value_as_int(t); //pegar os valores que serao alterados

    g_print("%d\n", valor); //imprimer no console o valor alterado
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    spin = GTK_WIDGET(gtk_builder_get_object(builder, "spinbutton1"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show_all(window);

    gtk_main();

    return (0);
}
