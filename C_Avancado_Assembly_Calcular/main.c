#include <stdio.h>
#include <stdlib.h>

int main()
{
    int numero1 = 10, numero2 = 5, soma, subtracao;

    asm("add %%ebx, %%eax;" : "=a" (soma) : "a" (numero1), "b" (numero2)); //add funcao para somar em assembly
    asm("sub %%ebx, %%eax;" : "=a" (subtracao) : "a" (numero1), "b" (numero2));  //sub funcao para subtrair em assembly

   //ebx usado para armazenar dados
   //eax usado para operacoes aritmeticas
    printf("Soma: %d\n", soma);
    printf("Subtracao: %d\n", subtracao);

    return 0;
}
