#include <stdio.h>
#include <stdlib.h>

//criar arvore vazia

struct NO{

    int numero;
    struct NO *direita;
    struct NO *esquerda;
    char letra;
};

typedef struct NO no;

no *CriarVazia(void){  //criar arvore vazia

    return NULL;
}

no *Criar(char c, no *direito, no *esquerdo){  //adicionar elementos na arvore

    no *aux = malloc(sizeof(no)); //alocar na memoria
    //parametos adcionados esquerdo e direito
    aux->letra = c;
    aux->direita = direito;
    aux->esquerda = esquerdo;
    return aux;
}

int Vazia(no *p){

    return p == NULL;
}

no *Libera(no *val){  //liberar elemento da arvore

    if(!Vazia(val)){
        Libera(val->esquerda);
        Libera(val->direita);
        free(val);
    }
    return NULL;
}

void Imprimir(no *val){

    if(!Vazia(val)){
        printf("\n%c\n", val->letra); //impimir no principal
        Imprimir(val->direita); //imprimir sub no a direita e esquerda
        //Imprimir(val->esquerda);
    }else{
        printf("\nArvore Vazia!\n");
    }
}

int main()
{
   no *v1 = Criar('b', CriarVazia(), CriarVazia()); //no folha porque nao existe nada ligado a ele
   no *v2 = Criar('a',v1, CriarVazia());
   v2->direita = Libera(v2->direita);

   Imprimir(v2);

   return 0;
}
