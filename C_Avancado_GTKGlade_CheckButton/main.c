#include <stdlib.h>
#include <gtk/gtk.h>

//Selecionar botoes e imformar no console.

GtkBuilder *builder;
GtkWidget *window;
GtkWidget *toggle1;
GtkWidget *toggle2;

G_MODULE_EXPORT void Seleciona1(gpointer t){ //verificar se os botoes foram selecionados e imprimir no console

    if(gtk_toggle_button_get_active(t) == 1){
        g_print("Opacao1 Selecionado");
    }else{
        g_print("Opacao1 Nao Selecionado");
    }
}

G_MODULE_EXPORT void Seleciona2(gpointer t){

    if(gtk_toggle_button_get_active(t) == 1){
        g_print("Opcao2 Selecionado");
    }else{
        g_print("Opcao2 Nao Selecionado");
    }
}

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "Janela"));
    toggle1 = GTK_WIDGET(gtk_builder_get_object(builder, "Opcao1")); //informar botao no glade
    toggle2 = GTK_WIDGET(gtk_builder_get_object(builder, "Opcao2"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(G_OBJECT(builder));
    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
