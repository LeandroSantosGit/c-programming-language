#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Acos */ //funcao de arco cosseno
/*
#define PI 3.14159265

int main()
{
    double valor, resultado;
    valor = 0.52;

    resultado = acos(valor) * 180 / PI; //obter valor do arco de cosseno

    printf("Arco Cosseno de valor - %f\n\n", resultado);

    return 0;
} */


/* Asin */ //funcao arco de seno
/*
#define PI 3.14159265;

int main(){

    double valor, resultado;
    valor = 0.52;

    resultado = asin(valor) * 180 / PI; //obter valor do arco de seno

    printf("Arc Sin de valor = %f\n\n", resultado);

    return 0;
} */


/* Atan */ //funcao de arco tangente

#define PI 3.14159265
/*
int main(){

    double valor, resultado;
    valor = 0.52;

    resultado = atan(valor) * 180 / PI; //obter valor do arco tangente

    printf("Arc Tan de valor = %f\n\n", resultado);

    return 0;
} */


/* Ceil */ //funcao de arredondar para cima determinado valor
/*
int main(){

    float valor1, valor2, valor3;
    valor1 = 1.5;
    valor2 = 2.2;
    valor3 = 2.6;

    printf("valor1 = %f\nvalor2 = %f\nvalor3 = %f\n", ceil(valor1), ceil(valor2), ceil(valor3));

    return 0;
} */


/* Cos */ //funcao de calcular cosseno
/*
int main(){

    double x = 60;

    printf("Cos de x = %f\n", cos(x)); //calcular cosseno

    return 0;
} */


/* Sin */ //funcao de calcular seno
/*
int main(){

    double x = 60;

    printf("Sin de x = %f\n", sin(x)); //calcular seno

    return 0;
} */


/* Tan */ //funcao de calcular tangente
/*
int main(){

    double x = 60;

    printf("Tan de x = %f\n", tan(x)); //calcular tangente

    return 0;
} */


/* Pow */ //funcao pra elevar um determinado numero a outro numero
/*
int main(){

    float x, y;
    x = 4.2;
    y = 2.0;

    printf("%f\n", pow(x, y)); //elevar primeiro numero elevado ao segundo

    return 0;
} */


/* Floor */ //funcao para arrendondar para baixo um determinado valor
/*
int main(){

    float x, y;
    x = 4.2;
    y = 2.9;

    printf("%f\n%f\n", floor(x), floor(y));

    return 0;
} */


/* Log */ //funcao de logaritimo
/*
int main(){

    double x, resultado;
    x = 2.7;

    resultado = log(x);

    printf("%lf\n", resultado);

    return 0;
} */


/* Fmod */ //funcao para mostrar o resto de uma divisao
/*
int main(){

    int x, y, r;
    x = 5;
    y = 2;
    r = fmod(x, y); //obter resto da divisao do primeiro e segundo valor

    printf("%d\n", r);

    return 0;
} */


/* Sqrt */ //funcao para calcular raiz quadrada
/*
int main(){

    int x, r;
    x = 4;
    r = sqrt(x); //calculando raiz quadrada

    printf("%d\n", r);

    return 0;
} */


/* Copysign */  //funcao que retorna o primeiro valor com o sinal do segundo
/*
int main(){

    float x, y;
    x = 27;
    y = -1;

    printf("%f\n", copysign(x,y));

    return 0;
} /*
