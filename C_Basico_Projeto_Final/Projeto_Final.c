#include<stdio.h>

void main(){

    int escolha;

    retorno:
        printf("\n\n\n");
        Menu();

        do{
            printf("\nDigite uma opcao do Menu: ");
            scanf("%i", &escolha);
        }while(escolha > 5);

            switch(escolha){
                case 0:
                    printf("\nSair\n");
                    exit(1);
                    break;

                case 1:
                    printf("\nDigite dois numeros para soma\n");
                    Soma();
                    goto retorno;
                    break;

                case 2:
                    printf("\nDigite dois numeros para Subtracao\n");
                    Subtracao();
                    goto retorno;
                    break;

                case 3:
                    printf("\nDigite dois numeros para divisao\n");
                    Divisao();
                    goto retorno;
                    break;

                case 4:
                    printf("\nDigite dois numeros para multiplicacao\n");
                    Multiplicacao();
                    goto retorno;
                    break;

                case 5:
                    printf("\nDigite dois numeros para calcular area\n");
                    Area();
                    goto retorno;
                    break;

                default:
                    printf("Opcao Invalida!");
                    goto retorno;
                    break;
            }
}

void Menu(){

    int i;

    //criar uma linha pra separar o menu
    for(i = 0; i < 50; i++){
        printf("-", i);
    }

    printf("\n\n       Menu         \n\n");

    printf("1 Soma \n");
    printf("2 Subtracao \n");
    printf("3 Divisao \n");
    printf("4 Multiplicacao \n");
    printf("5 Area do Triangulo \n\n");

    for(i = 0; i < 50; i++){
        printf("-");
    }
}

void Soma(){

    int x;
    int y;

    scanf("%i", &x); //ler oque o usuario digitar
    scanf("%i", &y);

    int resultado = x + y;
    printf("\nA soma entre os dois numeros: %i\n\n", resultado);
}

void Subtracao(){

    int x;
    int y;

    scanf("%i", &x);
    scanf("%i", &y);

    int resultado = x - y;
    printf("\nA subtracao entre os dois numeros: %i\n\n", resultado);
}

void Divisao(){

    int x;
    int y;

    scanf("%i", &x);
    scanf("%i", &y);

    if(y != 0){
        float resultado = (float) x / y;
        printf("\nA divisao entre os numeros: %.2f\n\n", resultado);
    }else{
        printf("Nao se pode dividir pra 0");
    }
}

void Multiplicacao(){

    int x;
    int y;

    scanf("%i", &x);
    scanf("%i", &y);

    float resultado = (float) x * y;
    printf("\nA multiplicacao entre os dois numeros: %.2f\n\n", resultado);
}

void Area(){

    int x;
    int y;

    scanf("%i", &x);
    scanf("%i", &y);

    float resultado = (float)(x * y) / 2;
    printf("\nA area do triangulo e: %.4f\n\n", resultado);
}
