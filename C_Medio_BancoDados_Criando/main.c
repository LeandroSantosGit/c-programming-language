#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

int main(int argc, char **argv)
{
  MYSQL *conn = mysql_init(NULL);  //ponteiro de conexao, mysql_init iniciar a estrutura mysql

  if (mysql_real_connect(conn, "localhost", "root", "", NULL, 0, NULL, 0) == NULL){ //verificar se foi possivel se conectar
      fprintf(stderr, "%s\n", mysql_error(conn)); //stderr direciona mensagem de erro para outros locais sem ser a saida padrao
      mysql_close(conn); //caso algum erro fechamento da conexao
      exit(1); //fechamento da aplicacao
}
             //CREATE e para criar banco
  if (mysql_query(conn, "CREATE DATABASE TW")){
      fprintf(stderr, "%s\n", mysql_error(conn));
      mysql_close(conn);
      exit(1);
  }

  mysql_close(conn);
  exit(0);
}
