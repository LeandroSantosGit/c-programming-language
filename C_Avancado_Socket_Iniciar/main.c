#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>

int main(int argc, char **argv)
{
    WSADATA wsa; //recebe a inicializacao do socket

    printf("\nInitalising Winsock..."); //Informar que inicializacao do socket
    //iniciar socket
    if(WSAStartup(MAKEWORD(1,1), &wsa) == SOCKET_ERROR){ //verificar se socket foi iniciado
        printf("Faleid. Error Code: %d", WSAGetLastError()); //caso nao iniciado imformar ao usuario
        return 1;
    }

    printf("Initialised.\n"); //caso iniciado imformar ao usuario

    return 0;
}
