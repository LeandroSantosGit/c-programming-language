#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    int *vetor;

    vetor = malloc(5* sizeof(int));

    if(!vetor){
        printf("Memoria esgotada!");
        exit(1);
    }

    printf("Digite os valores para vetor\n");

    for(i = 0; i < 5; i++){
        printf("Valor para o vetor:\n");
        scanf("%d", &vetor[i]);
    }
    printf("\nValores do vetor\n");

    for(i = 0; i < 5; i++){
        printf("Vetor[%d] - %d\n", i, vetor[i]);
    }

    vetor = realloc(vetor,2*sizeof(int));

    printf("\n\nDigite valores para novo vetor\n");

    for(i = 0; i < 2; i++){
        printf("Valor para vetor: \n");
        scanf("%d", &vetor[i]);
    }

    printf("\nValor do vetores\n");

    for(i = 0; i < 2; i++){
        printf("Vetor[%d] - %d\n", i, vetor[i]);
    }

    free(vetor);

    return 0;
}
