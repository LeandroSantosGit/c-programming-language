#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>

int main(int argc, char **argv)
{
    WSADATA wsa; //iniciar socket
    SOCKET sk; //criar socket

    printf("Initialising Winsock...\n"); //informar a inicializacao do socket
    if(WSAStartup(MAKEWORD(1, 1), &wsa) == SOCKET_ERROR){ //verificar se inicio socket
        printf("Falid. Error Code: %d", WSAGetLastError()); //case nao iniciar imprimir
        return 1;
    }

    printf("Initialised.\n"); //caso iniciado imprimir

    //criar socket
    if((sk = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET){
        printf("Cloud not create socket: %d", WSAGetLastError());
    }

    printf("Socket created.\n");

    return 0;
}
