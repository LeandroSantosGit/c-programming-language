#include <stdio.h>
#include <stdlib.h>

typedef struct Numeros{

    int numero1;
    int numero2;
}soma;

void Exibir(soma s){

    printf("\nNumero1: %d\n", s.numero1);
    printf("Numero2: %d\n", s.numero2);
    printf("Resultado da Soma: %d\n", s.numero1 + s.numero2);
}

int main()
{
    soma s1 = {10, 5};
    Exibir(s1);
    soma s2 = {8, 8};
    Exibir(s2);

    return 0;
}
