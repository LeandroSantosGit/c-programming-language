#include <stdio.h>
#include <winsock.h>
 //variaveis para inicializacao do socket
WSADATA data;
SOCKET winsock; //criacao do socket
SOCKADDR_IN sock; //contem a configuracao do socket, porta, IP

int main()
{
    if(WSAStartup(MAKEWORD(1,1), &data) == SOCKET_ERROR){  //inicializacao do uso do insock
        printf("Erro");
        return 0;
    }
                             //SOCK_STREAM protocolo TCP
    if((winsock = socket(AF_INET, SOCK_STREAM, 0)) == SOCKET_ERROR){  //inicializacao do socket
        printf("Nao Criou Socket");
        return 0;
    }

    printf("Socket iniciou");
    getch();
    closesocket(winsock);
    WSACleanup();
    return 0;
}
