#include <stdio.h>
#include <stdlib.h>

int main(){

    int quantidadeE;
    int i;
    int vetor[0];

    printf("Informe a quantidades de valores do vetor:\n");
    scanf("%d", &quantidadeE);

    printf("Digite os valores do vetor: \n");
    for(i = 0; i < quantidadeE; i++){
        printf("Valor para o vetor %d\n", i);
        scanf("%d", &vetor[i]);
    }

    printf("Valores do vetor\n");
    for(i = 0; i < quantidadeE; i++){
        printf("Vetor[%d] - %d \n", i, vetor[i]);
    }

    return 0;
}
