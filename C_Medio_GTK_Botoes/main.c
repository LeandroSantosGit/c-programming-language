#include <stdlib.h>
#include <gtk/gtk.h>

//usamos a funcao gtk_button_new pra adcionar botao
/*
int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *botao;

    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo");

    botao = gtk_button_new(); //criar botao
    gtk_button_set_label(GTK_WINDOW(botao), "Clique"); //adicionar nome ao botao
    gtk_container_add(GTK_WINDOW(janela), botao); //adicionar botao na janela

    gtk_widget_show(janela);
    gtk_widget_show(botao);

    gtk_main();
}

*/

//usamos a funcao gtk_button_new_with_label(gchar *label) pra adcionar botao

int main(int argc, char **argv){


    GtkWidget *janela;
    GtkWidget *botao;

    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo");

    botao = gtk_button_new_with_label("Clique"); //adicionar botao ja com nome
    gtk_container_add(GTK_CONTAINER(janela), botao);

    gtk_widget_show(janela);
    gtk_widget_show(botao);

    gtk_main();
}
