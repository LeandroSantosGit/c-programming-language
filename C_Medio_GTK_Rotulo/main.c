#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void Fechar(){

    gtk_main_quit();
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *rotulo; //ponteiro rotulo

    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Janela");
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    rotulo = gtk_label_new("Ola");  //criar rotulo
    gtk_label_set_selectable(rotulo, "true"); //deixar o conteudo do rotulo selecionavel
    gtk_container_add(GTK_CONTAINER(janela), rotulo); //adicionar rotulo na janela
    gtk_signal_connect(GTK_OBJECT(rotulo), "copy-clipboard", GTK_SIGNAL_FUNC(Fechar), NULL); /*fechar e para quando o rotulo
                    copy-clipbord ato de copiar conteudo do rotulo                       for copiado o aplicativo sera fechado*/
    gtk_widget_show(janela);
    gtk_widget_show(rotulo);

    gtk_main();

    return 0;
}
