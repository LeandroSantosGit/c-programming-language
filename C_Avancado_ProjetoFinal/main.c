#include <stdlib.h>
#include <gtk/gtk.h>

void Destruir(GtkWidget *widget, gpointer data){

    gtk_main_quit();
}

void FormatoBotao(GtkWidget *button, GtkTextBuffer *buffer){

    GtkTextIter start, end;
    gchar *tag_name;

    //retorna nossa selecao(iteradores) no buffer
    gtk_text_buffer_get_selection_bounds(buffer, &start, &end);
    //pega o nome da tag
    tag_name = g_object_get_data(G_OBJECT(button), "tag");
    //aplicarmos efeito no texto
    gtk_text_buffer_apply_tag_by_name(buffer, tag_name, &start, &end);
}

void FecharBotao(GtkWidget *button, GtkTextBuffer *buffer){

    GtkTextIter start;
    GtkTextIter end;

    gchar *text;

    gtk_text_buffer_get_bounds(buffer, &start, &end);
    //retorna o texto entra start e end
    text = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
    g_print("%s", text);
    g_free(text);
    gtk_main_quit();
}

int main(int argc, char *argv[]){

    GtkWidget *window;
    GtkWidget *vbox;
    GtkWidget *bbox;

    GtkWidget *NegritoBotao;
    GtkWidget *ItalicoBotao;
    GtkWidget *NormalBotao;

    GtkWidget *textView;
    GtkWidget *closebotao;
    GtkTextBuffer *buffer;

    gtk_init(&argc, &argv);
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Editor de Texto");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 400);
    g_signal_connect(G_OBJECT(window), "destroy", (Destruir), NULL);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    bbox = gtk_hbutton_box_new();
    gtk_box_pack_start(GTK_BOX(vbox), bbox, 0, 0, 0);

    textView = gtk_text_view_new();
    gtk_box_pack_start(GTK_BOX(vbox), textView, 1, 1, 0);
    buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textView));
    gtk_text_buffer_set_text(buffer, "Digite o Texto Aqui", -1);
    gtk_text_buffer_create_tag(buffer, "NEGRITO", "weight", PANGO_WEIGHT_BOLD, NULL);
    gtk_text_buffer_create_tag(buffer, "ITALICO", "style", PANGO_STYLE_ITALIC, NULL);
    gtk_text_buffer_create_tag(buffer, "NORMAL", "style", PANGO_STYLE_NORMAL, NULL);

    NegritoBotao = gtk_button_new_with_label("NEGRITO");
    gtk_container_add(GTK_CONTAINER(bbox), NegritoBotao);
    g_signal_connect(G_OBJECT(NegritoBotao), "clicked", (FormatoBotao), buffer);
    g_object_set_data(G_OBJECT(NegritoBotao), "tag", "NEGRITO");

    ItalicoBotao = gtk_button_new_with_label("ITALICO");
    gtk_container_add(GTK_CONTAINER(bbox), ItalicoBotao);
    g_signal_connect(G_OBJECT(ItalicoBotao), "clicked", (FormatoBotao), buffer);
    g_object_set_data(G_OBJECT(ItalicoBotao), "tag", "ITALICO");

    NormalBotao = gtk_button_new_with_label("NORMAL");
    gtk_container_add(GTK_CONTAINER(bbox), NormalBotao);
    g_signal_connect(G_OBJECT(NormalBotao), "clicked", (FormatoBotao), buffer);
    g_object_set_data(G_OBJECT(NormalBotao), "tag", "NORMAL");

    closebotao = gtk_button_new_with_label("Fechar");
    gtk_box_pack_start(GTK_BOX(vbox), closebotao, 0, 0, 0);
    g_signal_connect(G_OBJECT(closebotao), "clicked", (FecharBotao), buffer);

    gtk_widget_show_all(window);

    gtk_main();

    return 0;
}
