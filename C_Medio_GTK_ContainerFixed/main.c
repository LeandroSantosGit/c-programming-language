#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void Fechar(){

    gtk_main_quit();
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *fixedC;
    GtkWidget *botao1;
    GtkWidget *botao2;

    gtk_init(&argc, &argv);

    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Leandro");
    gtk_window_set_default_size(GTK_WINDOW(janela), 800, 600); //ajustar tamanho da janela
    gtk_window_set_position(GTK_WINDOW(janela), GTK_WIN_POS_CENTER); //colocar a janela no centro da tela do computador
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    fixedC = gtk_fixed_new(); //criar container
    gtk_container_add(GTK_CONTAINER(janela), fixedC); //adcionar container na janela

    botao1 = gtk_button_new_with_label("Botao1"); //criar botao ja com seu nome
    gtk_fixed_put(GTK_FIXED(fixedC), botao1, 0, 0);  //adcionar ao container e sua posicao
    gtk_widget_set_size_request(botao1, 80, 30); //definir tamanhos do botao

    botao2 = gtk_button_new_with_label("Botao2");
    gtk_fixed_put(GTK_FIXED(fixedC), botao2, 78, 0);
    gtk_widget_set_size_request(botao2, 80, 30);

    gtk_widget_show_all(janela); //exibir tudo oque foi adcionado na janela

    gtk_main();

    return 0;
}
