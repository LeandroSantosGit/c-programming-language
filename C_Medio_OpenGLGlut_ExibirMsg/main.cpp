#include <windows.h>
#include <C:\Program Files (x86)\CodeBlocks\MinGW\include\GL\glut.h>
#include <stdio.h>
#include <stdlib.h>

void Desenha(void){

    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}

void Mensagem(int val){

    printf("Ola\n");

    glutTimerFunc(2000, Mensagem, val);
}

void Inicializa(void){

    glClearColor(1.0f, 1.0f, 0.0f, 0.0f);
}

int main(void){

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 10);
    glutCreateWindow("Janela");
    glutDisplayFunc(Desenha);
    glutTimerFunc(2000, Mensagem, 1);
    Inicializa();
    glutMainLoop();
}
