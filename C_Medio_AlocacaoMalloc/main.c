#include <stdio.h>
#include <stdlib.h>

int main(){

    int *pointer;

    pointer = malloc(sizeof(int)); //alocando na memoria, sizeof e para obeter a quantidade de bytes
    //verifica se existe alocacao
    if(pointer){
        printf("Memoria alocada com sucesso\n");
    }else{
        printf("Memoria nao alocada\n");
        exit(1);
    }

    *pointer = 100; //atribuicao de valor

     printf("Valor: %d\n\n", *pointer);
     free(pointer);
     return 0;
}
