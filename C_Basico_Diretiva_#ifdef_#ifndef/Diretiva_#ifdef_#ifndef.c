#include<stdio.h>
#define MAX 100

void main(){

    #ifdef MAX  //se definido
        printf("Max foi definido com o valor: %i\n", MAX);
    #endif // MAX

    #undef MAX //retirar a definicao de MAX

    #ifndef MAX //se nao definido
        printf("\nMax nao foi definido\n\n");
    #endif

}
