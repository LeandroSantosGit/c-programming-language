#include <windows.h>
#include <C:\Program Files (x86)\CodeBlocks\MinGW\include\GL\glut.h>
#include <stdlib.h>
#include <stdio.h>

void Desenha(void){

    glMatrixMode(GL_MODELVIEW); //comunicar ao OpenGL todas as operacoes
    glLoadIdentity(); //iniciar sistema de coodenadas

    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0f, 1.0f, 1.0f); //define a cor do desenho que nesse caso e branca

    glBegin(GL_LINE_LOOP);  //definir o tipo de desenho
            glVertex2i(100, 150);
            glVertex2i(100, 100); //desenho de linhas
            glVertex2i(150, 100);
            glVertex2i(150, 150);
    glEnd(); //fechar a definicao do tipo de desenho
    glFlush();
}

void Iniciar(void){

    glClearColor(0.0f, 0.0f, 1.0f, 1.0f); //definida cor azul
}

void AjustaTamanho(GLsizei w, GLsizei h){  //ajustar janela e objeto desenhado

    glViewport(0, 0, w, h); //define area de desenho

    glMatrixMode(GL_PROJECTION); //informar ao OpenGL as alteracoes do moldes do desenho
    glLoadIdentity();

    gluOrtho2D(0.0f, 250.0f, 0.0f, 250.0f); //determina a projecao ortografica que vai ser usada para exibir desenho
}

//Programa Principal
int main(void){

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(800, 600); //determina tamanho da janela
    glutInitWindowPosition(400, 100); //determina a posicao que ira nascer a janela
    glutCreateWindow("Quadro");
    glutDisplayFunc(Desenha);
    glutReshapeFunc(AjustaTamanho); //chamar a funcao ajustar tamanho sempre que a janela for maximizada ou minimizada
    Iniciar();
    glutMainLoop();
}
