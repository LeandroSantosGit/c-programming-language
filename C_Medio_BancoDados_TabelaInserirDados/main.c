#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>

int main(int argc, char **argv){

    MYSQL *conn = mysql_init(NULL);

    if(mysql_real_connect(conn, "localhost", "root", "", "TW", 0, NULL, 0) == NULL){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }
       //inserir dados dentro da tabela no banco de dados TW
    if(mysql_query(conn, "INSERT INTO pessoas(Id, Nome, Sobrenome) VALUES(1, MARCOS, ALEXANDRE)")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "INSERT INTO pessoas(Id, Nome, Sobrenome) VALUES(2, CLAUDIA, MARQUES)")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    if(mysql_query(conn, "INSERT INTO pessoas(Id, Nome, Sobrenome) VALUES(3, FABRICIA, SILVA)")){
        fprintf(stderr, "%s\n", mysql_error(conn));
        mysql_close(conn);
        exit(1);
    }

    mysql_close(conn);
    exit(0);
}
