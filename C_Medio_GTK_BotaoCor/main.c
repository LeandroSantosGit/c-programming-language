#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void Fechar(){

    gtk_main_quit();
}

void Sair(){

    g_print("Tchau!!");
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *botaoCor; //ponteiro botao com cor

    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo");
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL);

    botaoCor = gtk_color_button_new(); //criar botao
    gtk_container_add(GTK_CONTAINER(janela), botaoCor); //adicionar botao na janela
    gtk_signal_connect(GTK_OBJECT(botaoCor), "leave", GTK_SIGNAL_FUNC(Sair), NULL);

    gtk_widget_show(janela);
    gtk_widget_show(botaoCor);

    gtk_main();

    return 0;
}
