#include <stdio.h>
#include <stdlib.h>

int main(){

    int quantidadeE;
    int i;
    int *vetor;

    printf("Informe a quantidade de vetores: ");
    scanf("%d", &quantidadeE);

    vetor = malloc(quantidadeE*sizeof(int)); //convetendo para unidade de memoria

    if(!vetor){
        printf("Memoria esgotada!\n");
        exit(1);
    }

    //usuario digitar os valores dos vetores
    printf("Digite os valores para o vetore: \n");
    for(i = 0; i < quantidadeE; i++){
        printf("Valor do vetor %d \n", i);
        scanf("%d",&vetor[i]);
    }

    //imprimir valor dos vetores
    printf("Valores do vetor\n");
    for(i = 0; i < quantidadeE; i++){
        printf("Vetor[%d] - %d\n", i, vetor[i]);
    }

    free(vetor);
    return 0;
}
