#include <windows.h>  //api do windows
#include <C:\Program Files (x86)\CodeBlocks\MinGW\include\GL\glut.h>  //caminho do glut.h no computador
#include <stdio.h>
#include <stdlib.h>

void Desenha(void){  //funcao para limpar buffer

    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}

void Iniciar(void){  //limpar janela com uma determinada cor

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f); //difine a cor
}

int main(void){

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); //informar Glut qual e o modo de exibicao da janela
    glutCreateWindow("Primeiro Programa"); //criar janela com determinado nome
    glutDisplayFunc(Desenha); //chamar funcao desenha
    Iniciar(); //chamar funcao inicializa que ira limpar janela
    glutMainLoop(); //processar tudo
}

