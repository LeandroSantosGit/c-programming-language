#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>

void CliqueBtao(){  //para mostra ao usuario que foi clicado no botao

    g_print("Clicou no botao\n");
}

void Fechar(){ //funcao para fechar aplicativo

    gtk_main_quit();
}

int main(int argc, char **argv){

    GtkWidget *janela;
    GtkWidget *botao;

    gtk_init(&argc, &argv);
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo");
    gtk_signal_connect(GTK_OBJECT(janela), "destroy", GTK_SIGNAL_FUNC(Fechar), NULL); //brecar o looping do gtk e fechar a janela

    botao = gtk_button_new_with_label("Clique");
    gtk_container_add(GTK_CONTAINER(janela), botao);
    gtk_signal_connect(GTK_OBJECT(botao), "clicked", GTK_SIGNAL_FUNC(CliqueBtao), NULL); //adicionar sinal ao botao

    gtk_widget_show(janela);
    gtk_widget_show(botao);

    gtk_main();

    return 0;
}
