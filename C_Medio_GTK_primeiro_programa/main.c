#include<stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>  //inclusao do gtk

int main(int argc, char **argv){

    GtkWidget *janela; //ponteiro para guardar referencia do nosso programa

    gtk_init(&argc, &argv); //executar as atribuicoes
    janela = gtk_window_new(GTK_WINDOW_TOPLEVEL); //criar a janela
    gtk_window_set_title(GTK_WINDOW(janela), "Ola Mundo"); //adcionar nome a nossa janela
    gtk_widget_show(janela);  //recebe como parametro o ponteiro widget janela
    gtk_main();
}
