#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>

#pragma comment(lib, "Ws2_32.lib") //Winsock Library

int main(int argc, char *argv[])
{
    WSADATA wsa;
    char *hostname = "www.facebook.com"; //endereco de site
    char ip[100]; //receber ip
    struct hostent *h;  //host
    struct in_addr **lista; //lista
    int i;

    printf("Iniciando Winsock...");
    if(WSAStartup(MAKEWORD(2, 2), &wsa) != 0){ //iniciar o Winsock
        printf("Erro Winsock %d", WSAGetLastError()); //se nao iniciar
        return 1;
    }

    if((h = gethostbyname(hostname)) == NULL){ //verificar a exitencia do hostname
        printf("gethostbyname Falhou: %d", WSAGetLastError());
        return 1;
    }

    lista = (struct in_addr **) h->h_addr_list; //

    for(i = 0; lista[i] != NULL; i++){
        strcpy(ip, inet_ntoa(*lista[i])); //receber o ip
    }

    printf("%s tem o ip: %s\n", hostname, ip); //imprimir o ip do site

    return 0;
}
