#include <stdlib.h>
#include <gtk/gtk.h>

GtkBuilder *builder;
GtkWidget *window;

int main(int argc, char **argv){

    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "Glade.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window1"));
    gtk_builder_connect_signals(builder, NULL);
    g_object_unref(builder);
    gtk_widget_show_all(window);

    gtk_main();

    return (0);
}
